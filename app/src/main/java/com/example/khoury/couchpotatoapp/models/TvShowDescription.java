package com.example.khoury.couchpotatoapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Khoury on 3/6/2018.
 */

public class TvShowDescription implements Serializable {

    private String name;

    @SerializedName("start_date")
    private String tvShowStartDate;

    @SerializedName("status")
    private String tvShowStatus;

    @SerializedName("runtime")
    private Integer tvShowRuntime;

    @SerializedName("rating")
    private Double tvShowRating;

    @SerializedName("description")
    private String tvShowDescription;

    @SerializedName("image_thumbnail_path")
    private String tvShowImageUrl;

    public String getName() {
        return name;
    }

    public String getTvShowStartDate() {
        return tvShowStartDate;
    }

    public String getTvShowStatus() {
        return tvShowStatus;
    }

    public Integer getTvShowRuntime() {
        return tvShowRuntime;
    }

    public Double getTvShowRating() {
        return tvShowRating;
    }

    public String getTvShowDescription() {
        return tvShowDescription;
    }

    public String getTvShowImageUrl() {
        return tvShowImageUrl;
    }
}
