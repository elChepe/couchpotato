package com.example.khoury.couchpotatoapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Khoury on 3/10/2018.
 */

public class PopularTvShowItem implements Serializable {

    @SerializedName("name")
    private String tvShowName;

    @SerializedName("image_thumbnail_path")
    private String imagePath;

    public String getName() {
        return tvShowName;
    }

    public String getImage_thumbnail_path() {
        return imagePath;
    }

}
