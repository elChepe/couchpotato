package com.example.khoury.couchpotatoapp.api;

import com.example.khoury.couchpotatoapp.models.PopularTvShowInfo;
import com.example.khoury.couchpotatoapp.models.TvShowInfo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Khoury on 3/6/2018.
 */

public interface TvShowApi {

    Integer pageNb = 2;

    @GET("show-details")
    Call<TvShowInfo> getTvShowByName(@Query("q") String tvShowName);

    @GET("most-popular")
    Call<PopularTvShowInfo> getPopularTvShows(@Query("page") Integer page);

}
