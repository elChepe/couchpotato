package com.example.khoury.couchpotatoapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Khoury on 3/10/2018.
 */

public class PopularTvShowInfo implements Serializable {

    @SerializedName("tv_shows")
    private List<PopularTvShowItem> popularTvShowItems;

    public List<PopularTvShowItem> getPopularTvShowItems() {
        return popularTvShowItems;
    }
}
