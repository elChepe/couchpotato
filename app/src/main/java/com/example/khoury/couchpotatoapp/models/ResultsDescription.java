package com.example.khoury.couchpotatoapp.models;

import java.io.Serializable;

/**
 * Created by Khoury on 3/6/2018.
 */

public class ResultsDescription implements Serializable {

    private String name;
    private String first_air_date;
    private String overview;
    private String original_language;

    public String getName() {
        return name;
    }

    public String getFirst_air_date() {
        return first_air_date;
    }

    public String getOverview() {
        return overview;
    }

    public String getOriginal_language() {
        return original_language;
    }
}
