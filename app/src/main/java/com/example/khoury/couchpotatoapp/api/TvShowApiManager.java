package com.example.khoury.couchpotatoapp.api;

import com.example.khoury.couchpotatoapp.models.PopularTvShowInfo;
import com.example.khoury.couchpotatoapp.models.TvShowInfo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Khoury on 3/6/2018.
 */

public class TvShowApiManager {

    private OkHttpClient okHttpClient;
    private Retrofit retrofit;
    private TvShowApi tvShowApi;

    private final String TVSHOW_BASE_URL = "https://www.episodate.com/api/";

    public TvShowApiManager() {
        Gson gson = new GsonBuilder().create();
        okHttpClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(new tvShowInterceptor())
                .build();

        retrofit = new Retrofit
                .Builder()
                .client(okHttpClient)
                .baseUrl(TVSHOW_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        tvShowApi = retrofit.create(TvShowApi.class);
    }

    public Call<TvShowInfo> getTvShowByName(String tvShowName) {
        tvShowName = tvShowName.replaceAll("\\s+", "-");

        return tvShowApi.getTvShowByName(tvShowName);
    }

    public Call<PopularTvShowInfo> getPopularTvShows() {
        return tvShowApi.getPopularTvShows(TvShowApi.pageNb);
    }

    private static class tvShowInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            HttpUrl originalUrl = request.url();
            HttpUrl modifiedUrl = originalUrl
                    .newBuilder()
                    .build();
            Request modifiedRequest = request.newBuilder().url(modifiedUrl).build();

            return chain.proceed(modifiedRequest);
        }
    }

}
