package com.example.khoury.couchpotatoapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Khoury on 3/6/2018.
 */

public class TvShowInfo implements Serializable {

    @SerializedName("tvShow")
    private TvShowDescription tvShowDescription;

    public TvShowDescription getTvShowDescription() {
        return tvShowDescription;
    }
}
