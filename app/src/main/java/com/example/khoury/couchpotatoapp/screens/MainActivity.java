package com.example.khoury.couchpotatoapp.screens;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.example.khoury.couchpotatoapp.R;
import com.example.khoury.couchpotatoapp.api.TvShowApiManager;
import com.example.khoury.couchpotatoapp.models.PopularTvShowInfo;
import com.example.khoury.couchpotatoapp.models.PopularTvShowItem;
import com.example.khoury.couchpotatoapp.models.TvShowInfo;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private final int TVSHOW_ACTIVITY_REQUEST_CODE = 200;
    private final int TVSHOW_ACTIVITY_REQUEST_CODE2 = 100;
    private String showName;


    public static final String TVSHOW_RESULT_KEY2 = "TVSHOW_RESULT_KEY2";

    private TvShowApiManager tvShowApiManager;
    private RecyclerView popularRecyclerView;

    private ImageView currentTvShowImageView;
    private TextView currentTvShowName, currentTvShowStartDate, currentTvShowStatus, currentTvShowRuntime, currentTvShowRating, currentTvShowDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvShowApiManager = new TvShowApiManager();

        popularRecyclerView = findViewById(R.id.popular_list);

        popularRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        getPopularTvShows();

        /*tvShowApiManager.getPopularTvShows().enqueue(new Callback<PopularTvShowInfo>() {
            @Override
            public void onResponse(Call<PopularTvShowInfo> call, Response<PopularTvShowInfo> response) {
                if (response.isSuccessful()) {
                    PopularTvShowInfo popularTvShowInfo = response.body();
                    if (popularTvShowInfo != null) {
                        showPopularTvShows(popularTvShowInfo);
                        hideLoadingAnimation();
                    }
                }
            }

            @Override
            public void onFailure(Call<PopularTvShowInfo> call, Throwable t) {
                hideLoadingAnimation();
            }
        });*/

    }

    /*private void getTvShowByName(String tvShowName) {
        tvShowApiManager.getTvShowByName(tvShowName).enqueue(new Callback<TvShowInfo>() {
            @Override
            public void onResponse(Call<TvShowInfo> call, Response<TvShowInfo> response) {
                if (response.isSuccessful()) {
                    TvShowInfo tvShowInfo = response.body();
                    if (tvShowInfo != null) {
                        showCurrentTvShow(tvShowInfo);
                    }
                }
            }

            @Override
            public void onFailure(Call<TvShowInfo> call, Throwable t) {

            }
        });
    }*/

    private void getPopularTvShows() {
        tvShowApiManager
                .getPopularTvShows().enqueue(new Callback<PopularTvShowInfo>() {
            @Override
            public void onResponse(Call<PopularTvShowInfo> call, Response<PopularTvShowInfo> response) {
                if (response.isSuccessful()) {
                    PopularTvShowInfo popularTvShowInfo = response.body();
                    if (popularTvShowInfo != null) {
                        showPopularTvShows(popularTvShowInfo);

                    }
                }
            }

            @Override
            public void onFailure(Call<PopularTvShowInfo> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.tvshows_menu:
                gotoTvShowSearchScreen();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TVSHOW_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            TvShowInfo tvShowInfo = (TvShowInfo) data.getSerializableExtra(TvShowSearchActivity.TVSHOW_RESULT_KEY);

            if (tvShowInfo != null) {
                gotoTvShowResultScreen(tvShowInfo.getTvShowDescription().getName());
            }
        }
    }

    private void gotoTvShowResultScreen(String name) {
        Intent intent = new Intent(this, TvShowResultActivity.class);
        intent.putExtra(TVSHOW_RESULT_KEY2, name);
        startActivity(intent);
    }

    private void setScreenTitle(String title) {
        if (!TextUtils.isEmpty(title)) {
            setTitle(title);
        }
    }

    /*private void showCurrentTvShow(TvShowInfo info) {

        //setContentView(R.layout.activity_result_display);

        String imageUrl = String.valueOf(info.getTvShowDescription().getTvShowImageUrl());
        Picasso.with(this).load(imageUrl).into(currentTvShowImageView);

        String currentTvShow = String.valueOf(info.getTvShowDescription().getName());
        currentTvShowName.setText(currentTvShow);
        setScreenTitle(info.getTvShowDescription().getName());

        String currentStartDate = String.valueOf(info.getTvShowDescription().getTvShowStartDate());
        currentTvShowStartDate.setText(currentStartDate);

        String currentStatus = String.valueOf(info.getTvShowDescription().getTvShowStatus());
        currentTvShowStatus.setText(currentStatus);

        String currentRuntime = String.valueOf(info.getTvShowDescription().getTvShowRuntime());
        currentTvShowStatus.setText(currentRuntime);

        String currentRating = String.valueOf(info.getTvShowDescription().getTvShowRating());
        currentTvShowRating.setText(currentRating);

        String currentDescription = String.valueOf(info.getTvShowDescription().getTvShowDescription());
        currentTvShowDescription.setText(currentDescription);

    }*/

    private void gotoTvShowSearchScreen() {
        Intent intent = new Intent(this, TvShowSearchActivity.class);
        startActivityForResult(intent, TVSHOW_ACTIVITY_REQUEST_CODE);
    }

    private void showPopularTvShows(PopularTvShowInfo info) {
        List<PopularTvShowItem> items = info.getPopularTvShowItems();
        PopularListAdapter adapter = new PopularListAdapter(items);
        popularRecyclerView.setAdapter(adapter);
    }

    public static class PopularListAdapter extends RecyclerView.Adapter<PopularListAdapter.ViewHolder> {

        private List<PopularTvShowItem> items;

        public PopularListAdapter(List<PopularTvShowItem> items) {
            this.items = items;
        }


        @Override
        public PopularListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tvshow_popular, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(PopularListAdapter.ViewHolder holder, int position) {
            Context context = holder.itemView.getContext();
            String tvShowName = items.get(position).getName();
            String imageUrl = items.get(position).getImage_thumbnail_path();

            holder.popularTextView.setText(tvShowName);
            Picasso.with(context).load(imageUrl).into(holder.popularImageView);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView popularImageView;
            TextView popularTextView;

            public ViewHolder(View itemView) {
                super(itemView);
                popularImageView = itemView.findViewById(R.id.image_thumbnail);
                popularTextView = itemView.findViewById(R.id.tvShow_Name);
            }

        }
    }

}
