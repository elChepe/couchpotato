package com.example.khoury.couchpotatoapp.screens;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.khoury.couchpotatoapp.R;
import com.example.khoury.couchpotatoapp.api.TvShowApiManager;
import com.example.khoury.couchpotatoapp.models.TvShowInfo;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Khoury on 3/11/2018.
 */

public class TvShowResultActivity extends AppCompatActivity {

    public static final String TVSHOW_RESULT_KEY = "TVSHOW_RESULT_KEY";

    private TvShowApiManager tvShowApiManager;
    private ImageView currentTvShowImageView;
    private TextView currentTvShowName, currentTvShowStartDate, currentTvShowStatus, currentTvShowRuntime, currentTvShowRating, currentTvShowDescription;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_display);

        currentTvShowImageView = findViewById(R.id.tvShow_Thumbnail);
        currentTvShowName = findViewById(R.id.tvShow_Name);
        currentTvShowStartDate = findViewById(R.id.tvShow_StartDate);
        //currentTvShowStatus = findViewById(R.id.tvShow_Status);
        //currentTvShowRuntime = findViewById(R.id.tvShow_Runtime);
        currentTvShowRating = findViewById(R.id.tvShow_Rating);
        currentTvShowDescription = findViewById(R.id.tvShow_Description);
        currentTvShowDescription.setMovementMethod(new ScrollingMovementMethod());
        progressBar = findViewById(R.id.progress_bar2);


        tvShowApiManager = new TvShowApiManager();

        String name = (String) getIntent().getSerializableExtra(MainActivity.TVSHOW_RESULT_KEY2);
        name = name.replaceAll("\\s+", "-");


        tvShowApiManager
                .getTvShowByName(name).enqueue(new Callback<TvShowInfo>() {
            @Override
            public void onResponse(Call<TvShowInfo> call, Response<TvShowInfo> response) {
                if (response.isSuccessful()) {
                    TvShowInfo tvShowInfo = response.body();
                    if (tvShowInfo != null) {
                        showCurrentTvShow(tvShowInfo);
                        //hideLoadingAnimation();

                    }
                }
                hideProgressBar();
            }

            @Override
            public void onFailure(Call<TvShowInfo> call, Throwable t) {
                //hideLoadingAnimation();
                hideProgressBar();
            }
        });
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    private void showCurrentTvShow(TvShowInfo info) {

        //setContentView(R.layout.activity_result_display);


        String imageUrl = String.valueOf(info.getTvShowDescription().getTvShowImageUrl());
        Picasso.with(this).load(imageUrl).into(currentTvShowImageView);

        String currentTvShow = String.valueOf(info.getTvShowDescription().getName());
        currentTvShowName.setText(currentTvShow);

        String currentStartDate = String.valueOf(info.getTvShowDescription().getTvShowStartDate());
        currentTvShowStartDate.setText(currentStartDate);

        /*String currentStatus = String.valueOf(info.getTvShowDescription().getTvShowStatus());
        currentTvShowStatus.setText(currentStatus);

        String currentRuntime = String.valueOf(info.getTvShowDescription().getTvShowRuntime());
        currentTvShowStatus.setText(currentRuntime);*/

        String currentRating = String.valueOf(info.getTvShowDescription().getTvShowRating());
        currentTvShowRating.setText(currentRating);

        String currentDescription = String.valueOf(info.getTvShowDescription().getTvShowDescription());
        currentTvShowDescription.setText(currentDescription);



    }

}
