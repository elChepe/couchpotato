package com.example.khoury.couchpotatoapp.screens;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.khoury.couchpotatoapp.R;
import com.example.khoury.couchpotatoapp.api.TvShowApiManager;
import com.example.khoury.couchpotatoapp.models.TvShowInfo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Khoury on 3/6/2018.
 */

public class TvShowSearchActivity extends AppCompatActivity {

    public static final String TVSHOW_RESULT_KEY = "TVSHOW_RESULT_KEY";

    private TvShowApiManager tvShowApiManager;
    private EditText tvshowNameEditText;
    private ImageView searchImageView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_display);

        tvShowApiManager = new TvShowApiManager();

        tvshowNameEditText = findViewById(R.id.tvShow_Name);
        searchImageView = findViewById(R.id.search_button);
        progressBar = findViewById(R.id.progress_bar);

        searchImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*Intent intent = new Intent();
                intent.putExtra(TVSHOW_RESULT_KEY, tvshowNameEditText.getText());
                startActivity(intent);*/

                attemptSearchForTvShow();
            }
        });
    }

    private void attemptSearchForTvShow() {
        showProgressBar();

        String currentText = tvshowNameEditText.getText().toString();
        currentText = currentText.replaceAll("\\s+", "-");
        /*Intent intent = new Intent(this, TvShowResultActivity.class);
        intent.putExtra(TVSHOW_RESULT_KEY, currentText);
        setResult(RESULT_OK, intent);
        finish();*/

        if (!TextUtils.isEmpty(currentText)) {
            tvShowApiManager
                    .getTvShowByName(currentText)
                    .enqueue(new Callback<TvShowInfo>() {
                        @Override
                        public void onResponse(Call<TvShowInfo> call, Response<TvShowInfo> response) {
                            if (response.isSuccessful()) {
                                TvShowInfo body = response.body();
                                Intent intent = new Intent();
                                intent.putExtra(TVSHOW_RESULT_KEY, body);
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                            hideProgressBar();
                        }

                        @Override
                        public void onFailure(Call<TvShowInfo> call, Throwable t) {
                            hideProgressBar();
                        }
                    });
        }

    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.INVISIBLE);
    }

}
